import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'selectItem.dart';
import 'package:qrscan/qrscan.dart' as scanner;
import 'package:dio/dio.dart';
import 'bloc/parents_bloc.dart';
import 'bloc/parents_event.dart';
import 'bloc/parents_state.dart';
import 'dart:async';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          home: MyHomePage(),
        ));
  }
}

class MyHomePage extends StatefulWidget {
  var listItem = [];

  bool check = true;

  MyHomePage({this.listItem, this.check = true});

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  ParentBloc _bloc = new ParentBloc();
  var arr1 = [
    {'name': '生育管理', 'img': 'images/growing-plant.png'},
    {'name': '資材管理', 'img': 'images/construction-machine.png'},
    {'name': '農薬管理', 'img': 'images/anti _insect.png'},
    {'name': '在庫管理', 'img': 'images/inventory.png'},
    {'name': '肥料管理', 'img': 'images/seed-bag.png'},
    {'name': '現場管理', 'img': 'images/field.png'},
    {'name': '設備点検', 'img': 'images/construction.png'},
    {'name': '苦情・Mtg', 'img': 'images/complaint.png'},
  ];
@override
  void initState() {
   _bloc.add(LoadParentEvent());

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: InkWell(
            child: Image.asset('images/code.png',scale: 20),
            onTap: (){
              _detectQrCode();
            },
          ),

          centerTitle: true,
          backgroundColor: Colors.greenAccent,
          title: Text("管理者向けアプリ"),
          actions: <Widget>[
            Padding(
              padding: const EdgeInsets.only(right: 8.0),
              child: InkWell(
                child:Image.asset('images/funnel.png',scale: 21,),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => SelectItem(),
                    ),
                  );
                },
              ),
            ),


          ],
        ),
        body: BlocBuilder(
          bloc: _bloc,
          builder: (context,ParentsState state){
            if(state is InitalParentsState){
              return Center(child: CupertinoActivityIndicator());

            }
            if(state is ParentsStateLoaded){
            }
            return widget.check ? _BuildBody() : _BuildBody1();
          },
        )



    );
  }

  _BuildBody() {
    final children = <Widget>[];
    int len = widget.check ? _bloc.listData.length : widget.listItem.length;
    for (var x = 0; x < len; x += 2) {
      children.add(
        widget.check
            ? Flexible(
          child: Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  child: InkWell(
                    onTap: (){},
                    child: Container(
                      decoration: BoxDecoration(
                          color:_bloc.listData[x].isInstall ? _bloc.listData[x].isRegistration? Colors.green:Colors.green.withOpacity(0.6):Colors.green.withOpacity(0.6),
                          borderRadius:
                          BorderRadius.all(Radius.circular(10))),
                      child: Column(
                        children: <Widget>[
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(top: 4.0),
                              child: Image.asset(
                                _bloc.listData[x].img,
                                color: Colors.white,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(bottom: 4.0),
                            child: Text(
                              _bloc.listData[x].name,
                              style: TextStyle(
                                  color: Colors.white, fontSize: 24),
                            ),
                          )
                        ],
                      ),
                    ),

                  )
                ),
                const SizedBox(width: 8.0),
                Expanded(
                  child: InkWell(
                    onTap: (){},
                    child: Container(
                      decoration: BoxDecoration(
                          color: _bloc.listData[x+1].isInstall ? _bloc.listData[x+1].isRegistration? Colors.green:Colors.green.withOpacity(0.6):Colors.green.withOpacity(0.6),
                          borderRadius:
                          BorderRadius.all(Radius.circular(10))),
                      child: Column(
                        children: <Widget>[
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(top: 4.0),
                              child: Image.asset(
                                _bloc.listData[x+1].img,
                                color: Colors.white,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(bottom: 4.0),
                            child: Text(
                              _bloc.listData[x + 1].name,
                              style: TextStyle(
                                  color: Colors.white, fontSize: 24),
                            ),
                          )
                        ],
                      ),
                    ),
                  )
                ),
              ],
            ),
          ),
        )
            : Flexible(
          child: Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius:
                        BorderRadius.all(Radius.circular(10))),
                    child: Column(
                      children: <Widget>[
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(top: 4.0),
                            child: Image.asset(
                              widget.listItem[x]['img'],
                              color: Colors.white,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 4.0),
                          child: Text(
                            widget.listItem[x]['name'],
                            style: TextStyle(
                                color: Colors.white, fontSize: 30),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                const SizedBox(width: 8.0),
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius:
                        BorderRadius.all(Radius.circular(10))),
                    child: Column(
                      children: <Widget>[
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(top: 4.0),
                            child: Image.asset(
                              arr1[x + 1]['img'],
                              color: Colors.white,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 4.0),
                          child: Text(
                            arr1[x + 1]['name'],
                            style: TextStyle(
                                color: Colors.white, fontSize: 30),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    }
    return Padding(
      padding: const EdgeInsets.only(
          top: 12.0, left: 12.0, right: 12.0, bottom: 4.0),
      child: Column(
        children: children,
      ),
    );
  }
  _BuildBody1() {
    final children = <Widget>[];
    for (int a1 = 0; a1 < widget.listItem.length; a1 += 2) {
      children.add(
        Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: Container(
            height: MediaQuery.of(context).size.height * 0.20,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  child: Container(
                      decoration: BoxDecoration(
                          color: Colors.green,
                          borderRadius: BorderRadius.all(Radius.circular(10))),
                      child: Column(
                        children: <Widget>[
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(top: 4.0),
                              child: Image.asset(
                                widget.listItem[a1]['img'],
                                color: Colors.white,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(bottom: 4.0),
                            child: Text(
                              widget.listItem[a1]['name'],
                              style:
                              TextStyle(color: Colors.white, fontSize: 24),
                            ),
                          )
                        ],
                      )),
                ),
                const SizedBox(width: 8.0),
                Expanded(
                  child: a1 + 1 < widget.listItem.length
                      ? Container(
                      decoration: BoxDecoration(
                          color: Colors.green,
                          borderRadius:
                          BorderRadius.all(Radius.circular(10))),
                      child: Column(
                        children: <Widget>[
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(top: 4.0),
                              child: Image.asset(
                                widget.listItem[a1 + 1]['img'],
                                color: Colors.white,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(bottom: 4.0),
                            child: Text(
                              widget.listItem[a1 + 1]['name'],
                              style: TextStyle(
                                  color: Colors.white, fontSize: 24),
                            ),
                          )
                        ],
                      ))
                      : Container(),
                )
              ],
            ),
          ),
        ),
      );
    }

//    int len = widget.k.length;
//    for(var x=0 ;x < len;x+=2){
//      children.add(Flexible(
//        child: Padding(
//          padding: const EdgeInsets.only(bottom: 8.0),
//          child: Row(
//            mainAxisAlignment: MainAxisAlignment.spaceBetween,
//            children: <Widget>[
//              Expanded(
//                child:  Container(
//                  decoration: BoxDecoration(
//                      color: Colors.green,
//                      borderRadius: BorderRadius.all(Radius.circular(10))
//                  ),
//                  child: Column(
//                    children: <Widget>[
//                      Expanded(
//                        child:Padding(
//                          padding: const EdgeInsets.only(top: 4.0),
//                          child: Image.asset(widget.k[x]['img'],color: Colors.white,),
//                        ),
//                      ),
//                      Padding(
//                        padding: const EdgeInsets.only(bottom: 4.0),
//                        child: Text(widget.k[x]['name'],style: TextStyle(color: Colors.white,fontSize: 30),),
//                      )
//                    ],),
//                ),
//              ),
//              const SizedBox(width: 8.0),
//              Expanded(
//                child:  Container(
//                  decoration: BoxDecoration(
//                      color: Colors.green,
//                      borderRadius: BorderRadius.all(Radius.circular(10))
//                  ),
//                  child: Column(
//                    children: <Widget>[
//                      Expanded(
//                        child:Padding(
//                          padding: const EdgeInsets.only(top: 4.0),
//                          child: Image.asset(arr1[x+1]['img'],color: Colors.white,),
//                        ),
//                      ),
//                      Padding(
//                        padding: const EdgeInsets.only(bottom: 4.0),
//                        child: Text(arr1[x+1]['name'],style: TextStyle(color: Colors.white,fontSize: 30),),
//                      )
//                    ],),
//                ),
//              ),
//            ],
//          ),
//        ),
//
//
//      ),
//      );
//    }
    return Padding(
      padding: const EdgeInsets.only(
          top: 12.0, left: 12.0, right: 12.0, bottom: 4.0),
      child: Column(
        children: children,
      ),
    );
  }
  void _detectQrCode() async {
    try {
      String scanResult = (await scanner.scan()).trim();
      if (scanResult.isEmpty) {
//        setState(() {
//          _qrCodeError = Messages.QRCodeIncorrect;
//        });
        return;
      }
      if (scanResult.contains("https://")) {
//        MasterData masterData = MasterData();
        String scanResultUri = scanResult.substring(8, scanResult.length);
        var splitQueryString = scanResultUri.split('?');
        if (splitQueryString.length == 1) {
//          setState(() {
//            _qrCodeError = Messages.QRCodeIncorrect;
//          });
          return;
        }
        var splitSlashString = splitQueryString[0].split('/');
        if (splitSlashString.length == 1) {
//          setState(() {
//            _qrCodeError = Messages.QRCodeIncorrect;
//          });
          return;
        }
        int i;
        for (i = splitSlashString.length - 1; i >= 0; i--) {
          if (splitSlashString[i].trim().isEmpty) {
            continue;
          }
          break;
        }
        String newKey = splitSlashString[i].trim();
        String newURL = splitSlashString[0].trim();
        print(newKey);
        print(newURL);
//        if (masterData.secretkey == newKey && masterData.url == newURL) {
//          return;
//        }
//        masterData.secretkey = newKey;
//        masterData.url = newURL;
//        SharedPreferences prefs = await SharedPreferences.getInstance();
//        List<bool> resultQrCodeScan = await Future.wait<bool>([
//          prefs.setString('url', masterData.url),
//          prefs.setString('key', masterData.secretkey)
//        ]);
//        if ((resultQrCodeScan[0] ?? false) && (resultQrCodeScan[1] ?? false)) {
//          setState(() {
//            print("Save private key $newKey and url $newURL success");
//            _qrCodeMessage = Messages.QRCodeSuccess;
//            qrCodeError = errorUpdateApp = "";
//          });
//        }
      } else {
//        setState(() {
//          _qrCodeError = Messages.QRCodeIncorrect;
//        });
      }
    } catch (e) {}
  }

}
