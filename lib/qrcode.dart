import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:qrscan/qrscan.dart' as scanner;

class Qrcode extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return _Qrcode();
  }

}
class _Qrcode extends State<Qrcode>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: InkWell(
        onTap: (){
          _detectQrCode();
        },
          child: Text('Qrcod'),
      ),
    );
  }
  void _detectQrCode() async {
    try {
      String scanResult = (await scanner.scan()).trim();
      if (scanResult.isEmpty) {
//        setState(() {
//          _qrCodeError = Messages.QRCodeIncorrect;
//        });
        return;
      }
      if (scanResult.contains("https://")) {
//        MasterData masterData = MasterData();
        String scanResultUri = scanResult.substring(8, scanResult.length);
        var splitQueryString = scanResultUri.split('?');
        if (splitQueryString.length == 1) {
//          setState(() {
//            _qrCodeError = Messages.QRCodeIncorrect;
//          });
          return;
        }
        var splitSlashString = splitQueryString[0].split('/');
        if (splitSlashString.length == 1) {
//          setState(() {
//            _qrCodeError = Messages.QRCodeIncorrect;
//          });
          return;
        }
        int i;
        for (i = splitSlashString.length - 1; i >= 0; i--) {
          if (splitSlashString[i].trim().isEmpty) {
            continue;
          }
          break;
        }
        String newKey = splitSlashString[i].trim();
        String newURL = splitSlashString[0].trim();
        print(newKey);
        print(newURL);
//        if (masterData.secretkey == newKey && masterData.url == newURL) {
//          return;
//        }
//        masterData.secretkey = newKey;
//        masterData.url = newURL;
//        SharedPreferences prefs = await SharedPreferences.getInstance();
//        List<bool> resultQrCodeScan = await Future.wait<bool>([
//          prefs.setString('url', masterData.url),
//          prefs.setString('key', masterData.secretkey)
//        ]);
//        if ((resultQrCodeScan[0] ?? false) && (resultQrCodeScan[1] ?? false)) {
//          setState(() {
//            print("Save private key $newKey and url $newURL success");
//            _qrCodeMessage = Messages.QRCodeSuccess;
//            qrCodeError = errorUpdateApp = "";
//          });
//        }
      } else {
//        setState(() {
//          _qrCodeError = Messages.QRCodeIncorrect;
//        });
      }
    } catch (e) {}
  }

}