import '../contains/base_url.dart';
import '../model/response/parents_screen_response.dart';

import 'base_provider.dart';

class ParentsScreenProvider extends BaseProvider {
  Future<ParentsScreenResponse> getListParents({String uri}) async {
    if (uri == null) {
      uri = BaseUrl.baseUrl;

    }
    var jsonData = await this.get(uri);
    return ParentsScreenResponse.fromJson(jsonData);
  }
}
