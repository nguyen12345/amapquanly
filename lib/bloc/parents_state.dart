abstract class ParentsState{
  ParentsState([List props = const []]) : super();
}
class InitalParentsState extends ParentsState{}
class ParentsStateLoading extends ParentsState{}
class ParentsStateLoaded extends ParentsState{}