import 'dart:core' as prefix0;
import 'dart:core';
import '../bloc/parents_event.dart';
import '../bloc/parents_state.dart';
import '../model/parents_screen.dart';
import '../provider/parents_screen_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ParentBloc extends Bloc<ParentEvent, ParentsState> {
  List<ParentsScreen> listData = [];
  String nextUrl = '';

  ParentsScreenProvider _parentsScreenProvider = new ParentsScreenProvider();

  @override
  get initialState => InitalParentsState();

  @override
  Stream<ParentsState> mapEventToState(ParentEvent event) async* {
    if (event is LoadParentEvent) {
      if (state is InitalParentsState) {
        var response = await _parentsScreenProvider.getListParents();
        if (response.status == "success") {
          this.listData = response.data;
          yield ParentsStateLoaded();
        }

      }
    }
  }
}
