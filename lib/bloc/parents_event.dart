abstract class ParentEvent  {
  ParentEvent([List props = const []]) : super();
}
class LoadParentEvent extends ParentEvent{}
class LoadMoreParentEvent extends ParentEvent{}