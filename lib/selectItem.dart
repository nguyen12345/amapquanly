import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'main.dart';
import 'package:toast/toast.dart';

class SelectItem extends StatefulWidget {
  @override
  _SelectItem createState() => _SelectItem();
}

class _SelectItem extends State<SelectItem> {
  var arr1 = [
    {'name': 'TOP画面に表示する項目をチェックしてください'},
    {'name': 'アプリ一覧'},
    {
      'name': '生育管理',
      'img': 'images/growing-plant.png',
      'package_name': 'dc',
      'check': false
    },
    {
      'name': '資材管理',
      'img': 'images/construction-machine.png',
      'package_name': 'abc',
      'check': false
    },
    {
      'name': '農薬管理',
      'img': 'images/anti _insect.png',
      'package_name': 'abc',
      'check': false
    },
    {
      'name': '在庫管理',
      'img': 'images/inventory.png',
      'package_name': '',
      'check': false
    },
    {
      'name': '肥料管理',
      'img': 'images/seed-bag.png',
      'package_name': '',
      'check': false
    },
    {
      'name': '現場管理',
      'img': 'images/field.png',
      'package_name': 'fb',
      'check': false
    },
    {
      'name': '設備点検',
      'img': 'images/construction.png',
      'package_name': '',
      'check': false
    },
    {
      'name': '苦情・Mtg',
      'img': 'images/complaint.png',
      'package_name': '',
      'check': false
    },
    {'name': '設定'},
  ];

  _loop() {
    final children = <Widget>[];
    for (var i = 0; i < arr1.length; i++) {
      if (i == 0) {
        children.add(Flexible(
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10))),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'TOP画面に表示する項目をチェックしてください',
                style: TextStyle(fontSize: 19, fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ));
      } else if (i == 1) {
        children.add(Flexible(
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10))),
            child: Padding(
              padding: const EdgeInsets.only(left: 8.0, top: 18.0),
              child: Text(
                'アプリ一覧',
                style: TextStyle(fontSize: 19, fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ));
      } else if (i == 10) {
        children.add(Flexible(
          child: Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: InkWell(
                onTap: () {
                  setState(() {
                    check = false;
                    listItem.clear();
                    if (arr1[2]['check'] == true) {
                      listItem.add(
                          {'name': '生育管理', 'img': 'images/growing-plant.png'});
                    }
                    if (arr1[3]['check'] == true) {
                      listItem.add({
                        'name': '資材管理',
                        'img': 'images/construction-machine.png'
                      });
                    }
                    if (arr1[4]['check'] == true) {
                      listItem.add(
                          {'name': '農薬管理', 'img': 'images/anti _insect.png'});
                    }
                    if (arr1[5]['check'] == true) {
                      listItem
                          .add({'name': '在庫管理', 'img': 'images/inventory.png'});
                    }
                    if (arr1[6]['check'] == true) {
                      listItem
                          .add({'name': '肥料管理', 'img': 'images/seed-bag.png'});
                    }
                    if (arr1[7]['check'] == true) {
                      listItem.add({'name': '現場管理', 'img': 'images/field.png'});
                    }
                    if (arr1[8]['check'] == true) {
                      listItem.add(
                          {'name': '設備点検', 'img': 'images/construction.png'});
                    }
                    if (arr1[9]['check'] == true) {
                      listItem.add(
                          {'name': '苦情・Mtg', 'img': 'images/complaint.png'});
                    }
                  });

                  if (listItem.length <= 0) {
                  } else {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => MyHomePage(
                                listItem: listItem,
                                check: check,
                              )),
                    );
                  }
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    const SizedBox(
                      width: 8.0,
                    ),
                    Expanded(
                      child: Container(
                          decoration: BoxDecoration(
                              color: Colors.green,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10))),
                          alignment: Alignment.center,
                          child: Text(
                            '設定',
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.bold),
                          )),
                    ),
                    const SizedBox(
                      width: 8.0,
                    ),
                  ],
                )),
          ),
        ));
      } else {
        children.add(Flexible(
          child: Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                arr1[i]['package_name'] == ''
                    ? Checkbox(
                        value: false,
                        onChanged: (bool value) {
                          setState(() {
                            Toast.show(
                                "The application is not installed", context,
                                duration: Toast.LENGTH_LONG,
                                gravity: Toast.BOTTOM);
                          });
                        },
                      )
                    : Checkbox(
                        value: arr1[i]['check'],
                        onChanged: (bool value) {
                          setState(() {
                            arr1[i]['check'] = value;
                          });
                        },
                      ),
                Padding(
                  padding: const EdgeInsets.only(top: 0.0, left: 16.0),
                  child: arr1[i]['package_name'] == ''
                      ? Text(
                          arr1[i]['name'],
                          style: TextStyle(
                              fontSize: 22,color: Colors.black38),
                        )
                      : Text(
                          arr1[i]['name'],
                          style: TextStyle(
                              fontSize: 22, fontWeight: FontWeight.bold),
                        ),
                )
              ],
            ),
          ),
        ));
      }
    }
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: children,
//        children: <Widget>[
//          Flexible(
//              child: Container(
//                decoration: BoxDecoration(
//
//                    borderRadius: BorderRadius.all(Radius.circular(10))),
//                child: Padding(
//                  padding: const EdgeInsets.all(8.0),
//                  child: Text(
//                    'TOP画面に表示する項目をチェックしてください',
//                    style: TextStyle(fontSize: 19,fontWeight: FontWeight.bold),
//                  ),
//                ),
//              ),
//
//
//          ),
//          Flexible(
//            child: Container(
//              decoration: BoxDecoration(
//                  borderRadius: BorderRadius.all(Radius.circular(10))),
//              child: Padding(
//                padding: const EdgeInsets.only(left: 8.0,top: 18.0),
//                child: Text(
//                  'アプリ一覧',
//                  style: TextStyle(fontSize: 19,fontWeight: FontWeight.bold),
//                ),
//              ),
//            ),
//
//
//          ),
//          Flexible(
//                child: Padding(
//                  padding: const EdgeInsets.only(top: 8.0),
//                  child: Row(
//                    crossAxisAlignment: CrossAxisAlignment.start,
//                    children: <Widget>[
//                      Checkbox(
//                        value: check1,
//                        onChanged: onChanged1,
//                      ),
//                      Padding(
//                        padding: const EdgeInsets.only(top: 8.0,left: 16.0),
//                        child: Text(
//                          '生育管理',
//                          style: TextStyle(fontSize: 22,fontWeight: FontWeight.bold),
//                        ),
//                      )
//                    ],
//                  ),
//                ),
//
//
//          ),
//          Flexible(
//            child: Padding(
//              padding: const EdgeInsets.only(top: 8.0),
//              child: Row(
//                crossAxisAlignment: CrossAxisAlignment.start,
//                children: <Widget>[
//                  Checkbox(
//                    value: check2,
//                    onChanged: onChanged2,
//                  ),
//                  Padding(
//                    padding: const EdgeInsets.only(top: 8.0,left: 16.0),
//                    child: Text(
//                      '資材管理',
//                      style: TextStyle(fontSize: 22,fontWeight: FontWeight.bold),
//                    ),
//                  )
//                ],
//              ),
//            ),
//
//
//          ),
//          Flexible(
//            child: Padding(
//              padding: const EdgeInsets.only(top: 8.0),
//              child: Row(
//                crossAxisAlignment: CrossAxisAlignment.start,
//                children: <Widget>[
//                  Checkbox(
//                    value: check3,
//                    onChanged: onChanged3,
//                  ),
//                  Padding(
//                    padding: const EdgeInsets.only(top: 8.0,left: 16.0),
//                    child: Text(
//                      '農薬管理',
//                      style: TextStyle(fontSize: 22,fontWeight: FontWeight.bold),
//                    ),
//                  )
//                ],
//              ),
//            ),
//
//
//          ),
//          Flexible(
//            child: Padding(
//              padding: const EdgeInsets.only(top: 8.0),
//              child: Row(
//                crossAxisAlignment: CrossAxisAlignment.start,
//                children: <Widget>[
//                  Checkbox(
//                    value: check4,
//                    onChanged: onChanged4,
//                  ),
//                  Padding(
//                    padding: const EdgeInsets.only(top: 8.0,left: 16.0),
//                    child: Text(
//                      '在庫管理',
//                      style: TextStyle(fontSize: 22,fontWeight: FontWeight.bold),
//                    ),
//                  )
//                ],
//              ),
//            ),
//
//
//          ),
//          Flexible(
//            child: Padding(
//              padding: const EdgeInsets.only(top: 8.0),
//              child: Row(
//                crossAxisAlignment: CrossAxisAlignment.start,
//                children: <Widget>[
//                  Checkbox(
//                    value: check5,
//                    onChanged: onChanged5,
//                  ),
//                  Padding(
//                    padding: const EdgeInsets.only(top: 8.0,left: 16.0),
//                    child: Text(
//                      '肥料管理',
//                      style: TextStyle(fontSize: 22,fontWeight: FontWeight.bold),
//                    ),
//                  )
//                ],
//              ),
//            ),
//
//
//          ),
//          Flexible(
//            child: Padding(
//              padding: const EdgeInsets.only(top: 8.0),
//              child: Row(
//                crossAxisAlignment: CrossAxisAlignment.start,
//                children: <Widget>[
//                  Checkbox(
//                    value: check6,
//                    onChanged: onChanged6,
//                  ),
//                  Padding(
//                    padding: const EdgeInsets.only(top: 8.0,left: 16.0),
//                    child: Text(
//                      '現場管理',
//                      style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),
//                    ),
//                  )
//                ],
//              ),
//            ),
//
//
//          ),
//          Flexible(
//            child: Padding(
//              padding: const EdgeInsets.only(top: 8.0),
//              child: Row(
//                crossAxisAlignment: CrossAxisAlignment.start,
//                children: <Widget>[
//                  Checkbox(
//                    value: check7,
//                    onChanged: onChanged7,
//                  ),
//                  Padding(
//                    padding: const EdgeInsets.only(top: 8.0,left: 16.0),
//                    child: Text(
//                      '設備点検',
//                      style: TextStyle(fontSize: 22,fontWeight: FontWeight.bold),
//                    ),
//                  )
//                ],
//              ),
//            ),
//
//
//          ),
//          Flexible(
//            child: Padding(
//              padding: const EdgeInsets.only(top: 8.0),
//              child: Row(
//                crossAxisAlignment: CrossAxisAlignment.start,
//                children: <Widget>[
//                  Checkbox(
//                    value: check8,
//                    onChanged: onChanged8,
//                  ),
//                  Padding(
//                    padding: const EdgeInsets.only(top: 8.0,left: 16.0),
//                    child: Text(
//                      '苦情・Mtg',
//                      style: TextStyle(fontSize: 22,fontWeight: FontWeight.bold),
//                    ),
//                  )
//                ],
//              ),
//            ),
//
//
//          ),
//          Flexible(
//            child: Padding(
//              padding: const EdgeInsets.only(top: 8.0),
//              child: InkWell(
//                onTap: () {
//                  setState(() {
//                    check = false;
//                    listItem.clear();
//                    if (check1 == true) {
//                      listItem.add({'name': '生育管理', 'img': 'images/growing-plant.png'});
//                    }
//                    if (check2 == true) {
//                      listItem.add({
//                        'name': '資材管理',
//                        'img': 'images/construction-machine.png'
//                      });
//                    }
//                    if (check3 == true) {
//                      listItem.add({'name': '農薬管理', 'img': 'images/anti _insect.png'});
//                    }
//                    if (check4 == true) {
//                      listItem.add({'name': '在庫管理', 'img': 'images/inventory.png'});
//                    }
//                    if (check5 == true) {
//                      listItem.add({'name': '肥料管理', 'img': 'images/seed-bag.png'});
//                    }
//                    if (check6 == true) {
//                      listItem.add({'name': '現場管理', 'img': 'images/field.png'});
//                    }
//                    if (check7 == true) {
//                      listItem.add({'name': '設備点検', 'img': 'images/construction.png'});
//                    }
//                    if (check8 == true) {
//                      listItem.add({'name': '苦情・Mtg', 'img': 'images/complaint.png'});
//                    }
//                  });
//
//                  if (listItem.length <= 0) {
//                  } else {
//                    Navigator.push(
//                      context,
//                      MaterialPageRoute(
//                          builder: (context) => MyHomePage(
//                            listItem: listItem,
//                            check: check,
//                          )),
//                    );
//                  }
//                },
//                child: Row(
//                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
//      children: <Widget>[
//   const SizedBox(width: 8.0,),
//        Expanded(
//          child: Container(
//              decoration: BoxDecoration(
//                  color: Colors.green,
//                borderRadius: BorderRadius.all(Radius.circular(10))
//              ),
//              alignment: Alignment.center,
//
//                child: Text('設定',style: TextStyle(fontSize: 22,fontWeight: FontWeight.bold),)),
//        ),
//        const SizedBox(width: 8.0,),
//
//      ],
//      )
//
//              ),
//            ),
//          )
//
//        ],
      ),
    );
  }

  bool check = true;
  var listItem = [];
  bool check1 = false;
  bool check2 = false;
  bool check3 = false;
  bool check4 = false;
  bool check5 = false;
  bool check6 = false;
  bool check7 = false;
  bool check8 = false;
  bool check9 = false;

  onChanged1(bool value) {
    setState(() {
      check1 = value;
    });
  }

  onChanged2(bool value) {
    setState(() {
      check2 = value;
    });
  }

  onChanged3(bool value) {
    setState(() {
      check3 = value;
    });
  }

  onChanged4(bool value) {
    setState(() {
      check4 = value;
    });
  }

  onChanged5(bool value) {
    setState(() {
      check5 = value;
    });
  }

  onChanged6(bool value) {
    setState(() {
      check6 = value;
    });
  }

  onChanged7(bool value) {
    setState(() {
      check7 = value;
    });
  }

  onChanged8(bool value) {
    setState(() {
      check8 = value;
    });
  }

  onChanged9(bool value) {
    setState(() {
      check9 = value;
    });
  }

//  _mang() {
//    return InkWell(
//      child: Container(
//          child: Column(
//        mainAxisAlignment: MainAxisAlignment.start,
//        crossAxisAlignment: CrossAxisAlignment.start,
//        children: <Widget>[
//          Text('TOP画面に表示する項目をチェックしてください'),
//          Text('アプリ一覧'),
//          Container(
//            child: Row(
//              crossAxisAlignment: CrossAxisAlignment.start,
//              children: <Widget>[
//                Checkbox(
//                  value: check1,
//                  onChanged: onChanged1,
//                ),
//                Text('生育管理')
//              ],
//            ),
//          ),
//          CheckboxListTile(
//            value: check2,
//            onChanged: onChanged2,
//            title: Text('資材管理'),
//          ),
//          CheckboxListTile(
//            value: check3,
//            onChanged: onChanged3,
//            title: Text('農薬管理'),
//          ),
//          CheckboxListTile(
//            value: check4,
//            onChanged: onChanged4,
//            title: Text('在庫管理'),
//          ),
//          CheckboxListTile(
//            value: check5,
//            onChanged: onChanged5,
//            title: Text('肥料管理'),
//          ),
//          CheckboxListTile(
//            value: check6,
//            onChanged: onChanged6,
//            title: Text('現場管理'),
//          ),
//          CheckboxListTile(
//            value: check7,
//            onChanged: onChanged7,
//            title: Text('設備点検'),
//          ),
//          CheckboxListTile(
//            value: check8,
//            onChanged: onChanged8,
//            title: Text('苦情・Mtg'),
//          ),
//          InkWell(
//            onTap: () {
//              setState(() {
//                check = false;
//                arr.clear();
//                if (check1 == true) {
//                  arr.add({'name': '生育管理', 'img': 'images/growing-plant.png'});
//                }
//                if (check2 == true) {
//                  arr.add({
//                    'name': '資材管理',
//                    'img': 'images/construction-machine.png'
//                  });
//                }
//                if (check3 == true) {
//                  arr.add({'name': '農薬管理', 'img': 'images/anti _insect.png'});
//                }
//                if (check4 == true) {
//                  arr.add({'name': '在庫管理', 'img': 'images/inventory.png'});
//                }
//                if (check5 == true) {
//                  arr.add({'name': '肥料管理', 'img': 'images/seed-bag.png'});
//                }
//                if (check6 == true) {
//                  arr.add({'name': '現場管理', 'img': 'images/field.png'});
//                }
//                if (check7 == true) {
//                  arr.add({'name': '設備点検', 'img': 'images/construction.png'});
//                }
//                if (check8 == true) {
//                  arr.add({'name': '苦情・Mtg', 'img': 'images/complaint.png'});
//                }
//              });
//
//              if (arr.length <= 0) {
//              } else {
//                Navigator.push(
//                  context,
//                  MaterialPageRoute(
//                      builder: (context) => MyHomePage(
//                            k: arr,
//                            check: check,
//                          )),
//                );
//              }
//            },
//            child: Text('設定'),
//          )
//        ],
//      )),
//    );
//  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('管理者向けアプリ'),
        ),
        body: SafeArea(child: _loop()));
  }
}
