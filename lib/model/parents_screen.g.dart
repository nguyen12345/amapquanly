// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'parents_screen.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ParentsScreen _$ParentsScreenFromJson(Map<String, dynamic> json) {
  return ParentsScreen(
    name: json['name'] as String,
    img: json['img'] as String,
    packageName:json['package_name'] as String,
    isInstall: json['is_install'] as bool,
    isRegistration: json['is_registration'] as bool,
  );
}

Map<String, dynamic> _$ParentsScreenToJson(ParentsScreen instance) =>
    <String, dynamic>{
      'name': instance.name,
      'img': instance.img,
      'package_name':instance.packageName,
      'is_install': instance.isInstall,
      'is_registration': instance.isRegistration,
    };
