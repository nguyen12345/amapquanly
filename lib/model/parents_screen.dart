import 'package:json_annotation/json_annotation.dart';
part 'parents_screen.g.dart';
@JsonSerializable()
class ParentsScreen {
  @JsonKey(name: 'name')
  String name;

  @JsonKey(name: 'img')
  String img;

  @JsonKey(name: 'is_install')
  bool isInstall;

  @JsonKey(name: 'is_registration')
  bool isRegistration;

  @JsonKey(name: 'package_name')
  String packageName;



  ParentsScreen({
    this.name,this.img,this.isInstall,this.isRegistration,this.packageName  });
  factory ParentsScreen.fromJson(Map<String,dynamic> json) => _$ParentsScreenFromJson(json);
  Map<String, dynamic> toJSON() => _$ParentsScreenToJson(this);

}
