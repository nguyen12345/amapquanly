// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'parents_screen_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ParentsScreenResponse _$ParentsScreenResponseFromJson(
    Map<String, dynamic> json) {
  return ParentsScreenResponse(
    data: (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : ParentsScreen.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  )
    ..status = json['status'] as String
    ..errorCode = json['errorCode'] as String
    ..message = json['message'] as String
    ..nextUrl = json['nextUrl'] as String;
}

Map<String, dynamic> _$ParentsScreenResponseToJson(
        ParentsScreenResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'errorCode': instance.errorCode,
      'message': instance.message,
      'nextUrl': instance.nextUrl,
      'data': instance.data,
    };
