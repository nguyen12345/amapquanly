import 'package:json_annotation/json_annotation.dart';
import '../parents_screen.dart';
import 'api_response.dart';
part 'parents_screen_response.g.dart';
@JsonSerializable()
class ParentsScreenResponse extends ApiResponse{
  List<ParentsScreen> data;
  ParentsScreenResponse({this.data});

  factory ParentsScreenResponse.fromJson(Map<String,dynamic> json) =>
      _$ParentsScreenResponseFromJson(json);

  Map<String, dynamic> toJSON() => _$ParentsScreenResponseToJson(this);
}
